<?php

class empleado {
    //TODO falta agregar imagen,agregar unidad a empleado en model,query
    private $connection;

    private function hashearPassword($Password){
        return password_hash($this->connection->real_escape_string($Password), PASSWORD_DEFAULT);
    }

    public function __construct(){
        $conn = new dbConnect();
        $this->connection = $conn->connect();
    }

    public function register($Nombre,$Apellidos,$Sexo,$Nacimiento,$Puesto,$EstadoCivil,$Curp,$Rfc,$Direccion,$Colonia,$Cp,$Telefono,$Celular,$Escolaridad,$Password){
        $query = $this->connection->prepare("insert into empleado(Nombre,Apellidos,Sexo,Nacimiento,Puesto,EstadoCivil,Curp,Rfc,Direccion,Colonia,Cp,Telefono,Celular,Escolaridad,Password)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $query->bind_param('ssssissssssssss',$Nombre,$Apellidos,$Sexo,$Nacimiento,$Puesto,$EstadoCivil,$Curp,$Rfc,$Direccion,$Colonia,$Cp,$Telefono,$Celular,$Escolaridad,$Password = $this->hashearPassword($Password));
        if($query->execute()){
            return json_encode(array('Codigo' => 0,'Msg' => 'Usuario registrado'));
        } else {
            return json_encode(array('Codigo' => 1,'Msg' => 'Error al registrar'));
        }
    }

    public function login($Curp,$Password){
        //$query = $this->connection->prepare("select idEmpleado,Nombre,Puesto,Rfc,Password from Empleado where Curp = ?;");
        $query = $this->connection->prepare("select idEmpleado,e.Nombre,p.Nombre,Rfc,Password from Empleado As e inner join Puestos As p on e.Puesto = p.idPuesto where Curp = ?;");
        $query->bind_param('s',$Curp);
        if($query->execute()){
            $query->bind_result($idEmpleado,$Nombre,$Puesto,$Rfc,$UserPassword);
            $query->fetch();
            if($idEmpleado){
                if(password_verify($Password,$UserPassword)){
                    return array('idEmpleado' => $idEmpleado, 'Nombre' => $Nombre, 'Puesto' => $Puesto, 'Curp' => $Curp);
                } else {
                    return false;
                }
            }
            return json_encode(array('Code' => 0,'Msg' => 'Usuario / contraseña invalidas'));
        }
    }

    public function mostrarEmpleados(){
        $query = $this->connection->query('select * from empleados;');
        $empleados = [];
        while($row = $query->fetch_assoc()){
            $empleados[] = array('id'=>$row['idEmpleado'],'Nombre'=>$row['Nombre'],'Apellidos'=>$row['Apellidos'],'Puesto'=>$row['Puesto']);
        }
        return json_encode($empleados);
    }

    public function detallesEmpleado($id){
        $query = $this->connection->prepare('call DetalleEmpleado(?);');
        $query->bind_param('i',$id);
        if($query->execute()){
            $result = $query->get_result();
            $row = $result->fetch_assoc();
            return json_encode(array('id'=>$row['idEmpleado'],'Nombre'=>$row['Nombre'],'Apellidos'=>$row['Apellidos'],'Sexo'=>$row['Sexo'],'Nacimiento'=>$row['Nacimiento'],'Puesto'=>$row['Puesto'],'EstadoCivil'=>$row['EstadoCivil'],'Curp'=>$row['Curp'],'Rfc'=>$row['Rfc'],'Direccion'=>$row['Direccion'],'Colonia'=>$row['Colonia'],'Cp'=>$row['Cp'],'Telefono'=>$row['Telefono'],'Celular'=>$row['Celular'],'Escolaridad'=>$row['Escolaridad']));
        }
    }
}