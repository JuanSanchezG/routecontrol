<?php


class puestos
{
    private $connection;

    public function __construct(){
        $conn = new dbConnect();
        $this->connection = $conn->connect();
    }

    public function obtenerPuestos(){
        $query = $this->connection->query('select * from puestos;');
        $puestos = [];
        while($row = $query->fetch_assoc()){
            $puestos[] = array('idPuesto' => $row['idPuesto'],'Nombre' => $row['Nombre']);
        }
        return json_encode($puestos);
    }
}