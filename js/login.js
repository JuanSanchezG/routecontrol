document.addEventListener('DOMContentLoaded',() => {
   document.getElementById('Login').addEventListener('click',() => {
       iniciarSesion();
   }) ;

   function iniciarSesion(){
       const data = new FormData();
       data.append('Rfc', document.getElementById('Rfc').value);
       data.append('Password', document.getElementById('Password').value);
       fetch('login',{
           body: data,
           method: 'POST'
       })
           .then(res => res.json())
           .then(res => {
               res.Code === 0 ? window.location = 'Inicio' : M.toast({ html: 'Usuario o contraseña incorrecto ', classes: 'red white-text' });
           });
   }
});