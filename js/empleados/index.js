document.addEventListener('DOMContentLoaded', () => {
    mostrarUsuarios();
    document.addEventListener('click',(e) => {
       if(e.target.classList.contains('detalles')){
           detalles(e.target.id);
       }
    });

    function mostrarUsuarios(){
        fetch('mostrarEmpleados')
            .then(res => res.json())
            .then(res => {
                const tablaEmpleados = document.getElementById('tablaEmpleados');
                res.map(x => {
                   tablaEmpleados.insertAdjacentHTML('beforeend',`
                        <tr>
                            <td>${x.Nombre} ${x.Apellidos}</td>
                            <td>${x.Puesto}</td>
                            <td>wip</td>
                            <td><a class="btn waves-effect waves-light detalles" id="${x.id}">Detalles</a></td>
                        </tr>
                   `);
                });
            })
    }

    function detalles(id){
        const data = new FormData();
        data.append('id',id);
        fetch('detallesEmpleado',{
            method: 'POST',
            body: data
        })
            .then(res => res.json())
            .then(res => {
                document.getElementById('detalleUsuario').innerHTML = `
                    <p>Nombre: ${res.Nombre} ${res.Apellidos}</p>
                    <p>Puesto: ${res.Puesto}</p>
                    <p>Estado civil: ${res.EstadoCivil}</p>
                    <p>Curp: ${res.Curp}</p>
                    <p>Rfc: ${res.Rfc}</p>
                    <p>Dirección: ${res.Direccion}</p>
                    <p>Colonia: ${res.Colonia}</p>
                    <p>Código postal: ${res.Cp}</p>
                    <p>Teléfono: ${res.Telefono}</p>
                    <p>Celular: ${res.Celular}</p>
                    <p>Escolaridad: ${res.Escolaridad}</p>
                `;
            })
    }
});