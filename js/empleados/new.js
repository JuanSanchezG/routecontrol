document.addEventListener('DOMContentLoaded', () => {
    const today = new Date();
    M.Datepicker.init(document.getElementById('Nacimiento'),{
        format: 'yyyy-mm-dd',
        maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });
    cargarPuestos();
    document.getElementById('Agregar').addEventListener('click', () => {
        AgregarEmpleado();
    });
    document.addEventListener('click',(e) => {
        if(e.target.classList.contains('puesto')){
            document.getElementById('puestos').value = e.target.attributes.value.value;
            document.getElementById('puestos').textContent = e.target.textContent;
        }
    });

    function AgregarEmpleado(){
        const data = new FormData();
        data.append('Nombre', document.getElementById('Nombre').value);
        data.append('Apellidos',document.getElementById('Apellidos').value);
        data.append('Sexo',document.getElementById('Sexo').value);
        data.append('Nacimiento',document.getElementById('Nacimiento').value);
        data.append('Puesto',document.getElementById('puestos').value);
        data.append('EstadoCivil',document.getElementById('EstadoCivil').value);
        data.append('Curp',document.getElementById('Curp').value);
        data.append('Rfc',document.getElementById('Rfc').value);
        data.append('Direccion',document.getElementById('Direccion').value);
        data.append('Colonia',document.getElementById('Colonia').value);
        data.append('Cp',document.getElementById('Cp').value);
        data.append('Telefono',document.getElementById('Telefono').value);
        data.append('Celular',document.getElementById('Celular').value);
        data.append('Escolaridad',document.getElementById('Escolaridad').value);
        data.append('Password',document.getElementById('Password').value);
        fetch('agregarEmpleado',{
            method: 'POST',
            body: data
        })
            .then(res => res.json())
            .then(res => {
                if(res.Codigo === 0){
                    M.toast({ html: res.Msg, classes: 'green white-text' });
                    document.getElementById('agregarEmpleado').reset();
                } else {
                    M.toast({ html: res.Msg, classes: 'red white-text' });
                }
            });
    }

    function cargarPuestos(){
        fetch('obtenerPuestos')
            .then(res => res.json())
            .then(res => {
                const dropdownPuestos = document.getElementById('listaPuestos');
                res.map(x => {
                    dropdownPuestos.insertAdjacentHTML('beforeend',`
                        <li><a value="${x.idPuesto}" class="puesto">${x.Nombre}</a></li>
                    `);
                });
                M.Dropdown.init(document.getElementById('puestos'));
            })
    }
});