<?php
require_once 'models/empleado.php';
require_once 'models/puestos.php';

class empleadosController
{
    function new(){
        $_SESSION['url'] = 'Agregar empleado';
        require_once 'views/empleados/new.php';
    }

    function index(){
        $_SESSION['url'] = 'Empleados';
        require_once 'views/empleados/index.php';
    }

    function agregarEmpleado(){
        if($_POST){
            $registrar = new empleado();
            echo $registrar->register($_POST['Nombre'],$_POST['Apellidos'],$_POST['Sexo'],$_POST['Nacimiento'],$_POST['Puesto'],$_POST['EstadoCivil'],$_POST['Curp'],$_POST['Rfc'],$_POST['Direccion'],$_POST['Colonia'],$_POST['Cp'],$_POST['Telefono'],$_POST['Celular'],$_POST['Escolaridad'],$_POST['Password']);
        }
    }

    function obtenerPuestos(){
        $Puestos = new puestos();
        echo $Puestos->obtenerPuestos();
    }

    function mostrarEmpleados(){
        $empleados = new empleado();
        echo $empleados->mostrarEmpleados();
    }

    function detallesEmpleado(){
        $empleado = new empleado();
        echo $empleado->detallesEmpleado($_POST['id']);
    }
}