<?php
    require_once 'models/empleado.php';
    class loginController {

        public function index(){
            require_once 'views/login.php';
        }

        public function register(){
            $employee = new empleado();
            $employee->register();
        }

        public function login(){
            $employee = new empleado();
            $identity =  $employee->login($_POST['Rfc'],$_POST['Password']);
            if($identity){
                $_SESSION['identity'] = $identity;
                echo json_encode(array('Code' => 0, 'msg' => 'Sesión iniciada'));
            } else {
                echo json_encode(array('Code' => 1, 'msg' => 'Usuario o contraseña incorrectos'));
            }
        }

        public function Inicio(){
            header('Location: ' . base_url . 'inicio/index');
        }
    }