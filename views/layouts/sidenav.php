<?php
require_once 'views/layouts/header.php';
?>
<header>
    <nav class="top-nav">
        <div class="nav-container">
            <div class="nav-wrapper">
                <div class="row">
                    <div class="col s12 m10 offset-m1">
                        <h1 class="header"><?= $_SESSION['url']?></h1>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <ul class="sidenav sidenav-fixed">
        <li>
            <div class="user-view">
                <div class="background">
                    <img src="../images/background.png" class="responsive-img">
                </div>
                img
                <a><span class="white-text name"><?= $_SESSION['identity']['Nombre'] ?></span></a>
                <a><span class="white-text name"><?= $_SESSION['identity']['Puesto'] ?></span></a>
            </div>
        </li>
        <?php if (strcmp('Administrador', $_SESSION['identity']['Puesto']) == 0): ?>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header waves-effect waves-teal">Empleados<i class="material-icons">group</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="<?= base_url ?>empleados/index">Mostrar Empleados <i class="material-icons">people_alt</i></a></li>
                                <li><a href="<?= base_url ?>empleados/new">Agregar <i class="material-icons">person_add</i></a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header waves-effect waves-teal">Usuarios</a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="">Mostrar Usuarios</a></li>
                                <li><a href="">Agregar</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header waves-effect waves-teal">Informes</a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="">Ver informes</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
        <?php endif ?>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a class="collapsible-header waves-effect waves-teal">Servicios</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="">Mostrar servicios</a></li>
                            <li><a href="">Agregar</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a class="collapsible-header waves-effect waves-teal">Unidades</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="">Mostrar unidades</a></li>
                            <?php if (strcmp('Administrador', $_SESSION['identity']['Puesto']) == 0): ?>
                                <li><a href="">Agregar</a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a class="collapsible-header waves-effect waves-teal">Rutas</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="">Mostrar rutas</a></li>
                            <?php if (strcmp('Administrador', $_SESSION['identity']['Puesto']) == 0): ?>
                                <li><a href="">Crear</a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</header>