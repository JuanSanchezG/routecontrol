<?php
if ($_SESSION['identity'] == null) {
    header('Location: ' . base_url . 'login/index');
}
require_once 'views/layouts/sidenav.php';
require_once 'views/layouts/footer.php';