<?php
require_once 'views/layouts/sidenav.php';
?>
<main>
    <div class="row">
        <div class="col s12 m9">
            <table class="centered">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Unidad</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="tablaEmpleados"></tbody>
            </table>
        </div>
        <div class="col s12 m3 detalleUsuario" id="detalleUsuario">
            Click para detalles
        </div>
    </div>
</main>
<?php require_once 'views/layouts/footer.php'; ?>
<script src="<?= base_url ?>js/empleados/index.js"></script>
