<?php
require_once 'views/layouts/sidenav.php';
?>
<main>
    <div class="container">
        <div class="row">
            <form id="agregarEmpleado" class="col s12">
                <div class="card-panel">
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" id="Nombre" class="required" required>
                            <label for="Nombre">Nombre <span class="red-text"> *</span></label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" id="Apellidos" class="required" required>
                            <label for="Apellidos">Apellidos<span class="red-text"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" id="Sexo" class="required" required>
                            <label for="Sexo">Sexo<span class="red-text"> *</span></label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" id="Nacimiento" class="datepicker" required>
                            <label for="Nacimiento">Fecha de nacimiento<span class="red-text"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <a class="dropdown-trigger btn" id="puestos" data-target="listaPuestos">Seleccione puesto</a>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" id="EstadoCivil">
                            <label for="EstadoCivil">Estado civil</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" id="Curp" class="required" required>
                            <label for="Curp">Curp<span class="red-text"> *</span></label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" id="Rfc" class="required" required>
                            <label for="Rfc">Rfc<span class="red-text"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" id="Direccion" class="required" required>
                            <label for="Direccion">Dirección<span class="red-text"> *</span></label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input type="text" id="Colonia" class="required" required>
                            <label for="Colonia">Colonia<span class="red-text"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input type="text" id="Cp">
                            <label for="Cp">Codigo postal</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input type="text" id="Telefono">
                            <label for="Telefono">Telefono</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input type="text" id="Celular" class="required" required>
                            <label for="Celular">Celular<span class="red-text"> *</span></label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input type="text" id="Escolaridad">
                            <label for="Escolaridad">Escolaridad</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input type="password" id="Password" class="required" required>
                            <label for="Password">Contraseña<span class="red-text"> *</span></label>
                        </div>
                        <a class="btn waves-effect waves-light" id="Agregar">Agregar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
<ul id="listaPuestos" class="dropdown-content">

</ul>
<?php require_once 'views/layouts/footer.php'; ?>
<script src="<?= base_url ?>js/empleados/new.js"></script>
</html>