<?php
include_once 'views/layouts/header.php';
session_destroy();
?>
<body>
<div class="container">
    <form id="formLogin">
        <div class="card-panel">
            <h4 class="center-align">Iniciar Sesión</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" id="Rfc" name="Rfc">
                    <label for="Rfc">RFC</label>
                </div>
                <div class="input-field col s12">
                    <input type="password" id="Password" name="Password">
                    <label for="Password">Contraseña</label>
                </div>
            </div>
            <a class="waves-effect waves-light btn" id="Login">Iniciar Sesión</a>
        </div>
    </form>
</div>
</body>
<?= include_once 'views/layouts/footer.php' ?>
<script src="<?= base_url ?>js/login.js"></script>
